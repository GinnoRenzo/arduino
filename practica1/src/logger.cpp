// FIFO tuberias con nombre
//reciba del juego tenis una cadena de texto cada vez que alguno de los dos jugadores anota un punto.

#include <sys/types.h> //uso de librería FIFO
#include <sys/stat.h>   //uso de librería FIFO
#include <errno.h> //mensajes de error
#include <stdio.h> //entrada salida de texto
#include <stdlib.h> //gestion de memoria dinámica y control de procesos
#include <string.h> //
#include  <fcntl.h>
#include <unistd.h>

int main()
{
 char buf[1024];
 mkfifo("/tmp/mi_fifo",0777);
 int fd=open("/tmp/mi_fifo",O_RDONLY);
	if(fd<0)
	{
	 perror("hay que abrirla bien");
	}
	while(1)
	{
	 read(fd,buf,sizeof(buf));
	 printf("%s\n",buf);
	 if(strcmp(buf,"Fin del programa")==0)break;
	}
 close(fd);
 unlink("/tmp/mi_fifo");
return 0;
}
