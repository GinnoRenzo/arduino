// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <sys/types.h> //uso de librería FIFO
#include <sys/stat.h>   //uso de librería FIFO
#include <errno.h> //mensajes de error
#include <stdio.h> //entrada salida de texto
#include <stdlib.h> //gestion de memoria dinámica y control de procesos
#include <string.h> //
#include  <fcntl.h>
#include <unistd.h>
#include  <sys/mman.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	
//identificador de FIFO comunicación con logger
	//int fd;
	//char buf[1000];
//atributo de memoria  compartida comunicación con bot
	DatosMemCompartida datos;
	DatosMemCompartida *pdatos;
//identificador de comunicación tubería coordenadas entre cliente---->servidor
	int fdcs;
//identificador de comunicación tubería tecla cliente---->servidor
	int fdt;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
